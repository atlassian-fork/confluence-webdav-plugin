# Confluence WebDAV Plugin

Adds WebDAV support to Confluence. Provides a `<davResourceFactory>` module type other plugins may use to publish WebDAV resources. Also ships with a default resource factory that exposes Confluence spaces / content / revisions / attachments as WebDAV resources.

### Builds

Located in [CONFPLGTRK-WBDVTRUNK](https://confluence-bamboo.internal.atlassian.com/browse/CONFPLGTRK-WBDVTRUNK) on CBAC.

### Ownership

This plugin does not have a clear owner. PRs reviewers can be selected from the fortunate few who most recently touched this plugin.

Setup the following properties when running in IDE,

-Dwebdav.host=localhost -Dhttp.port=8080 -Dwebdav.port=8080 -Dwebdav.context=confluence -Dwebdav.resource.path.prefix=/plugins/servlet/confluence/default
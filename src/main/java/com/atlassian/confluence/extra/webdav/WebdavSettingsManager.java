package com.atlassian.confluence.extra.webdav;

import java.util.Set;

public interface WebdavSettingsManager {
    WebdavSettings getWebdavSettings();

    boolean isClientInWriteBlacklist(String userAgent);

    Set<String> getWriteBlacklistClients();

    boolean isContentExportsResourceEnabled();

    boolean isContentVersionsResourceEnabled();

    boolean isContentUrlResourceEnabled();

    boolean isStrictPageResourcePathCheckingDisabled();

    void save(WebdavSettings webdavSettings);
}

package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.io.UnsupportedEncodingException;

/**
 * Represents the text file in the spaces directory (regardless whether the space is personal or global).
 * The text file represents the description of the space.
 *
 * @author weiching.cher
 */
public class SpaceContentResourceImpl extends AbstractTextContentResource {
    public static final String DISPLAY_NAME_SUFFIX = ".txt";

    private final SpaceManager spaceManager;

    private final String spaceKey;

    public SpaceContentResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport SettingsManager settingsManager,
            @ComponentImport SpaceManager spaceManager,
            String spaceKey) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, settingsManager);
        this.spaceManager = spaceManager;
        this.spaceKey = spaceKey;
    }

    public Space getSpace() {
        return spaceManager.getSpace(spaceKey);
    }

    public boolean exists() {
        return super.exists()
                && !((ConfluenceDavSession) getSession()).getResourceStates().isSpaceDescriptionHidden(getSpace());
    }

    protected byte[] getTextContentAsBytes(String encoding) throws UnsupportedEncodingException {
        return StringUtils.defaultString(getSpace().getDescription().getBodyContent().getBody()).getBytes(encoding);
    }

    public long getModificationTime() {
        return getSpace().getLastModificationDate().getTime();
    }

    protected long getCreationtTime() {
        return getSpace().getCreationDate().getTime();
    }

    public String getDisplayName() {
        return getSpace().getName() + DISPLAY_NAME_SUFFIX;
    }
}

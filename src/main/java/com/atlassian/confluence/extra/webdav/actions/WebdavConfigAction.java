package com.atlassian.confluence.extra.webdav.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.webdav.WebdavSettings;
import com.atlassian.confluence.extra.webdav.WebdavSettingsManager;
import org.apache.commons.lang.StringEscapeUtils;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class WebdavConfigAction extends ConfluenceActionSupport {
    private String[] denyRegexes;

    private boolean contentUrlResourceEnabled;

    private boolean contentExportsResourceEnabled;

    private boolean contentVersionsResourceEnabled;

    private boolean disableStrictPathCheck;

    private boolean hiddenOptionsEnabled;

    private WebdavSettingsManager webdavSettingsManager;

    public void setWebdavSettingsManager(WebdavSettingsManager webdavSettingsManager) {
        this.webdavSettingsManager = webdavSettingsManager;
    }

    public String[] getDenyRegexes() {
        return null == denyRegexes ? new String[0] : denyRegexes;
    }

    public void setDenyRegexes(String[] denyRegexes) {
        this.denyRegexes = denyRegexes;
    }

    public boolean isContentUrlResourceEnabled() {
        return contentUrlResourceEnabled;
    }

    public void setContentUrlResourceEnabled(boolean contentUrlResourceEnabled) {
        this.contentUrlResourceEnabled = contentUrlResourceEnabled;
    }

    public boolean isContentExportsResourceEnabled() {
        return contentExportsResourceEnabled;
    }

    public void setContentExportsResourceEnabled(boolean contentExportsResourceEnabled) {
        this.contentExportsResourceEnabled = contentExportsResourceEnabled;
    }

    public boolean isContentVersionsResourceEnabled() {
        return contentVersionsResourceEnabled;
    }

    public void setContentVersionsResourceEnabled(boolean contentVersionsResourceEnabled) {
        this.contentVersionsResourceEnabled = contentVersionsResourceEnabled;
    }

    public boolean isDisableStrictPathCheck() {
        return disableStrictPathCheck;
    }

    public void setDisableStrictPathCheck(boolean disableStrictPathCheck) {
        this.disableStrictPathCheck = disableStrictPathCheck;
    }

    public boolean isHiddenOptionsEnabled() {
        return hiddenOptionsEnabled;
    }

    public void setHiddenOptionsEnabled(boolean hiddenOptionsEnabled) {
        this.hiddenOptionsEnabled = hiddenOptionsEnabled;
    }

    public String execute() {
        WebdavSettings settings = getWebdavSettings();

        settings.setExcludedClientUserAgentRegexes(Arrays.asList(getDenyRegexes()));
        settings.setContentUrlResourceEnabled(isContentUrlResourceEnabled());
        settings.setContentExportsResourceEnabled(isContentExportsResourceEnabled());
        settings.setContentVersionsResourceEnabled(isContentVersionsResourceEnabled());
        settings.setStrictPageResourcePathCheckingDisabled(isDisableStrictPathCheck());

        webdavSettingsManager.save(settings);

        addActionMessage(getText("webdav.config.saved"));

        return SUCCESS;
    }

    public String doDefault() {
        WebdavSettings settings = getWebdavSettings();
        Set<String> excludedClientUserAgentRegexes = settings.getExcludedClientUserAgentRegexes();

        setDenyRegexes(excludedClientUserAgentRegexes.toArray(new String[excludedClientUserAgentRegexes.size()]));
        setContentUrlResourceEnabled(settings.isContentUrlResourceEnabled());
        setContentExportsResourceEnabled(settings.isContentExportsResourceEnabled());
        setContentVersionsResourceEnabled(settings.isContentVersionsResourceEnabled());
        setDisableStrictPathCheck(settings.isStrictPageResourcePathCheckingDisabled());

        return SUCCESS;
    }

    public WebdavSettings getWebdavSettings() {
        return webdavSettingsManager.getWebdavSettings();
    }

    public void validate() {
        super.validate();

        String[] deniedRegexes = getDenyRegexes();

        if (null != deniedRegexes && deniedRegexes.length > 0) {
            Set<String> invalidRegexes = new LinkedHashSet<String>();

            for (String deniedRegex : deniedRegexes) {
                try {
                    Pattern.compile(deniedRegex);
                } catch (PatternSyntaxException e) {
                    invalidRegexes.add(deniedRegex);
                }
            }

            if (!invalidRegexes.isEmpty()) {
                StringBuffer stringBuffer = new StringBuffer("<ul>");

                for (String invalidRegex : invalidRegexes)
                    stringBuffer.append("<li>").append(StringEscapeUtils.escapeHtml(invalidRegex)).append("</li>");

                stringBuffer.append("</ul>");

                addFieldError("denyRegexes", getText("webdav.config.denywrite.error.invalidregex", new String[]{stringBuffer.toString()}));
            }
        }
    }
}

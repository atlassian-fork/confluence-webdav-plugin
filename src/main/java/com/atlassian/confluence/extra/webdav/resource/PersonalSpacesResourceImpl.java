package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpaceType;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Represents the <tt>Personal</tt> directory. Its immediate children
 * will be directories representing personal spaces.
 *
 * @author weiching.cher
 */
public class PersonalSpacesResourceImpl extends AbstractCollectionResource {
    public static final String DISPLAY_NAME = "Personal";

    private final SpaceManager spaceManager;

    public PersonalSpacesResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport SpaceManager spaceManager) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
        this.spaceManager = spaceManager;
    }

    protected long getCreationtTime() {
        return 0;
    }

    public String getDisplayName() {
        return DISPLAY_NAME;
    }

    protected Collection<DavResource> getMemberResources() {
        try {
            Collection<DavResource> memberResources = new ArrayList<DavResource>();
            DavResourceLocator locator = getLocator();

            @SuppressWarnings("unchecked")
            List<Space> personalSpaces = spaceManager.getAllSpaces(
                    SpacesQuery.newQuery().forUser(AuthenticatedUserThreadLocal.getUser()).withSpaceType(SpaceType.PERSONAL).build());

            for (Space space : personalSpaces) {
                DavResourceLocator spaceResourceLocator;

                StringBuilder resourcePathBuffer = new StringBuilder();
                resourcePathBuffer.append("/" + DISPLAY_NAME + "/").append(space.getKey());

                spaceResourceLocator = locator.getFactory().createResourceLocator(
                        locator.getPrefix(), locator.getWorkspacePath(), resourcePathBuffer.toString(), false);

                memberResources.add(getFactory().createResource(spaceResourceLocator, getSession()));
            }

            return memberResources;
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }
}

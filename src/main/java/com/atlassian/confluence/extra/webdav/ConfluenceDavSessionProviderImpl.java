package com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.extra.webdav.util.WebdavConstants;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.SeraphUtils;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.AuthenticatorException;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.user.User;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavSessionProvider;
import org.apache.jackrabbit.webdav.WebdavRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Provides a {@link org.apache.jackrabbit.webdav.DavSession} based on user credentials.
 * The {@link org.apache.jackrabbit.webdav.DavSession} is pretty much like the
 * {@link javax.servlet.http.HttpSession} to web applications.
 *
 * @author weiching.cher
 */
@Component
@ExportAsService(DavSessionProvider.class)
public class ConfluenceDavSessionProviderImpl implements DavSessionProvider {
    private static Logger log = Logger.getLogger(ConfluenceDavSessionProviderImpl.class);

    private final UserAccessor userAccessor;

    private final ConfluenceDavSessionStore confluenceDavSessionStore;

    @Autowired
    public ConfluenceDavSessionProviderImpl(@ComponentImport UserAccessor userAccessor, ConfluenceDavSessionStore confluenceDavSessionStore) {
        this.userAccessor = userAccessor;
        this.confluenceDavSessionStore = confluenceDavSessionStore;
    }

    protected String[] getCredentialTokens(HttpServletRequest httpServletRequest)
            throws IOException, DavException {
        String[] authorizationHeaderTokens = StringUtils.split(
                StringUtils.trim(httpServletRequest.getHeader(WebdavConstants.HEADER_AUTHORIZATION)), ' ');
        String authorizationHeader;

        if (null == authorizationHeaderTokens)
            throw new DavException(HttpServletResponse.SC_UNAUTHORIZED, "Need authentication");

        if (authorizationHeaderTokens.length < 2)
            throw new IOException("Malformed Authorization header: " + httpServletRequest.getHeader(WebdavConstants.HEADER_AUTHORIZATION));

        if (StringUtils.isBlank(authorizationHeader = authorizationHeaderTokens[1]))
            throw new IOException("Unable to read Authorization header.");

        String userNameAndPassword =
                new String(
                        Base64.decodeBase64(authorizationHeader.getBytes("UTF-8")),
                        "UTF-8"
                );
        int indexOfColon = userNameAndPassword.indexOf(':');
        if (indexOfColon > 0) {
            // User name is the start of the authorization header value till the colon index (exclusive).
            String userName = userNameAndPassword.substring(0, indexOfColon);
            // Password is colon index + 1 up until the end of the authorization header value. If the colon is at the end of the value, the password is empty.
            String password = indexOfColon < userNameAndPassword.length() - 1 ? userNameAndPassword.substring(indexOfColon + 1) : "";
            return new String[]{userName, password};
        } else {
            return new String[0];
        }
    }

    protected String getUserName(HttpServletRequest httpServletRequest) throws IOException, DavException {
        String[] credentialTokens = getCredentialTokens(httpServletRequest);

        return credentialTokens.length == 2 ? credentialTokens[0] : null;
    }

    protected String getPassword(HttpServletRequest httpServletRequest) throws IOException, DavException {
        String[] credentialTokens = getCredentialTokens(httpServletRequest);

        return credentialTokens.length == 2 ? credentialTokens[1] : null;
    }

    protected ConfluenceDavSession getConfluenceDavSession(HttpServletRequest httpServletRequest)
            throws DavException {
        try {
            ConfluenceDavSession davSession = (ConfluenceDavSession) httpServletRequest.getSession().getAttribute(ConfluenceDavSession.class.getName());

            if (null == davSession) {
                log.debug("ConfluenceDavSession not found in HttpSession. Trying AuthenticatedUserThreadLocal.");

                User user = AuthenticatedUserThreadLocal.getUser();
                if (user != null) {
                    log.debug(new StringBuilder("Found user ").append(user.getName()).append(" in AuthenticatedUserThreadLocal. Returning a new ConfluenceDavSession based on it.").toString());
                    return new ConfluenceDavSession(user.getName());
                } else {
                    try {
                        String userName = getUserName(httpServletRequest);
                        String password = getPassword(httpServletRequest);

                        log.debug(new StringBuilder("Trying to find an existing session for ").append(userName).append(" with md5hex password ").append(DigestUtils.md5Hex(StringUtils.defaultString(password))).toString());

                        if (StringUtils.isNotEmpty(userName)
                                && StringUtils.isNotEmpty(password)
                                && authenticateWithSeraphAuthenticator(userName, password))
                            davSession = getConfluenceDavSessionFromSessionMap(userName);

                    } catch (AuthenticatorException ae) {
                        log.error("Unable to authenticate using the configured Seraph authenticator.", ae);
                        throw new DavException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ae);
                    } catch (IOException ioe) {
                        log.error("Unable to get user name and/or password from the Authenticate header.", ioe);
                        throw new DavException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ioe);
                    }
                }
            }

            return davSession;
        } catch (ClassCastException cce) {
            /* Plugin was upgraded so the class could no longer be case due to loading from a different ClassLoader */
            httpServletRequest.getSession().removeAttribute(ConfluenceDavSession.class.getName());
            return null;
        }
    }

    protected void setConfluenceDavSessionIntoHttpSession(HttpServletRequest httpServletRequest, ConfluenceDavSession confluenceDavSession) {
        httpServletRequest.getSession().setAttribute(
                ConfluenceDavSession.class.getName(),
                confluenceDavSession);
    }

    private boolean authenticateWithSeraphAuthenticator(String userName, String password) throws AuthenticatorException {
        SecurityConfig securityConfig = SeraphUtils.getConfig(ServletActionContext.getRequest());
        if (null != securityConfig) {
            Authenticator authenticator = securityConfig.getAuthenticator();

            boolean authenticated = authenticator.login(
                    ServletActionContext.getRequest(),
                    ServletActionContext.getResponse(),
                    userName,
                    password
            );

            log.debug(new StringBuilder("Authenticating as ")
                    .append(userName)
                    .append(" with md5hex password ").append(DigestUtils.md5Hex(StringUtils.defaultString(password)))
                    .append(" by ").append(authenticator.getClass().getName())
                    .append(" results in ").append(authenticated).toString());

            return authenticated;
        }

        log.error("Unable to get an Authenticator from Seraph.");
        return false;
    }

    private ConfluenceDavSession authenticate(HttpServletRequest httpServletRequest) throws DavException {
        try {
            String userName = getUserName(httpServletRequest);
            String password = getPassword(httpServletRequest);

            log.debug(new StringBuilder("User name: ").append(userName).append(", password: ").append(DigestUtils.md5Hex(StringUtils.defaultString(password))).toString());

            if (null == userName)
                throw new DavException(HttpServletResponse.SC_UNAUTHORIZED, "User name not specified.");

            if (null == password)
                throw new DavException(HttpServletResponse.SC_UNAUTHORIZED, "Password not specified.");

            if (authenticateWithSeraphAuthenticator(userName, password))
                return new ConfluenceDavSession(userName);
            else
                throw new DavException(HttpServletResponse.SC_UNAUTHORIZED, "Bad user name or password.");
        } catch (AuthenticatorException ae) {
            throw new DavException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ae);
        } catch (IOException ioe) {
            throw new DavException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ioe);
        }
    }

    public boolean attachSession(WebdavRequest request) throws DavException {
        ConfluenceDavSession confluenceDavSession = getConfluenceDavSession(request);

        if (null == confluenceDavSession) {
            log.debug("Looks like this request is not authenticated. We'll try to authenticate our user now.");
            confluenceDavSession = authenticate(request);
        }

        confluenceDavSession.setUserAgent(request.getHeader(WebdavConstants.HEADER_USER_AGENT));
        confluenceDavSession.updateActivityTimestamp();
        confluenceDavSession.setCurrentlyBeingUsed(true);

        setConfluenceDavSessionIntoSessionMap(confluenceDavSession);
        setConfluenceDavSessionIntoHttpSession(request, confluenceDavSession);
        request.setDavSession(confluenceDavSession);
        AuthenticatedUserThreadLocal.setUser(userAccessor.getUser(confluenceDavSession.getUserName()));

        return true;
    }

    public void releaseSession(WebdavRequest request) {
        ConfluenceDavSession confluenceDavSession = (ConfluenceDavSession) request.getDavSession();
        if (null != confluenceDavSession) {
            confluenceDavSession.setCurrentlyBeingUsed(false);
            /* To update the session map cache - Modified cached objects need to be put-back. */
            setConfluenceDavSessionIntoSessionMap(confluenceDavSession);
        }


        AuthenticatedUserThreadLocal.setUser(null);
        request.setDavSession(null);
    }

    private void setConfluenceDavSessionIntoSessionMap(ConfluenceDavSession confluenceDavSession) {
        confluenceDavSessionStore.mapSession(confluenceDavSession, confluenceDavSession.getUserName());
    }

    protected ConfluenceDavSession getConfluenceDavSessionFromSessionMap(String userName) {
        return confluenceDavSessionStore.getSession(userName);
    }
}

package com.atlassian.confluence.extra.webdav;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ModuleType(ListableModuleDescriptorFactory.class)
public class DefaultDavResourceFactoryModuleDescriptorFactory extends SingleModuleDescriptorFactory<DefaultDavResourceFactoryModuleDescriptor> {
    /**
     * Constructs an instance using a specific host container
     *
     * @param hostContainer The host container to use to create descriptor instances
     */
    @Autowired
    public DefaultDavResourceFactoryModuleDescriptorFactory(HostContainer hostContainer) {
        super(hostContainer, "davResourceFactory", DefaultDavResourceFactoryModuleDescriptor.class);
    }
}

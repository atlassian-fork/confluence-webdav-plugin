package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceIterator;
import org.apache.jackrabbit.webdav.DavResourceIteratorImpl;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.io.OutputContext;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.apache.jackrabbit.webdav.lock.SupportedLock;

import java.io.IOException;
import java.util.Collections;

public class NonExistentResource extends AbstractConfluenceResource {
    public NonExistentResource(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
    }

    protected long getCreationtTime() {
        return 0;
    }

    protected SupportedLock getSupportedLock() {
        return null; /* Locking not supported */
    }

    public boolean exists() {
        return false;
    }

    public boolean isCollection() {
        return false;
    }

    public String getDisplayName() {
        String[] resourcePathTokens = StringUtils.split(getResourcePath(), '/');

        return resourcePathTokens.length >= 1
                ? resourcePathTokens[resourcePathTokens.length - 1]
                : "/";
    }

    public void spool(OutputContext outputContext) throws IOException {
        outputContext.setContentLength(0);
        outputContext.setModificationTime(getModificationTime());
    }

    public DavResourceIterator getMembers() {
        return new DavResourceIteratorImpl(Collections.EMPTY_LIST);
    }
}


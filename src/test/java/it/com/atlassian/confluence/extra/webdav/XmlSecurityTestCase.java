package it.com.atlassian.confluence.extra.webdav;

import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.client.methods.PropFindMethod;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

public class XmlSecurityTestCase extends AbstractWebDavTestCase {

    private static final String xml = "<?xml version=\"1.0\"?>\n" +
            "<!DOCTYPE lolz [\n" +
            "  <!ENTITY lol \"lol\">\n" +
            "  <!ENTITY lol1 \"&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;\">\n" +
            "  <!ENTITY lol2 \"&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;\">\n" +
            "  <!ENTITY lol3 \"&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;\">\n" +
            "  <!ENTITY lol4 \"&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;\">\n" +
            "  <!ENTITY lol5 \"&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;\">\n" +
            "  <!ENTITY lol6 \"&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;\">\n" +
            "  <!ENTITY lol7 \"&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;\">\n" +
            "  <!ENTITY lol8 \"&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;\">\n" +
            "  <!ENTITY lol9 \"&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;\">\n" +
            "]>\n" +
            "<lolz>&lol9;</lolz>";

    // see CONFDEV-7519 for details
    public void testSecurity() throws IOException, DavException, SAXException, ParserConfigurationException {
        // Propfind demonstration space.txt
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        propFindMethod.setRequestEntity(new StringRequestEntity(xml, "text/xml", "UTF-8"));
        try {
            // this method will never return if the pluging does not correctly handle billion laughs attack (see CONFDEV-7519 for details) 
            // http://en.wikipedia.org/wiki/Billion_laughs
            httpClient.getHttpConnectionManager().getParams().setSoTimeout((int) TimeUnit.MINUTES.toMillis(1));
            httpClient.executeMethod(propFindMethod);
        } catch (SocketTimeoutException e) {
            fail("There is no protection against billion laughs attack.");
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }
}

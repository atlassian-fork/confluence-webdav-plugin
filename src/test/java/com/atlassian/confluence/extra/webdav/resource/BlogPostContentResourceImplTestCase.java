package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.spaces.Space;
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.webdav.io.OutputContext;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BlogPostContentResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    private Space space;
    private BlogPost blogPost;
    private Calendar calendar;

    private Integer bloggingYear;
    private Integer bloggingMonth;
    private Integer bloggingDay;

    private BlogPostContentResourceImpl blogPostContentResourceImpl;

    @Before
    public void initialise() {
        space = new Space("ds");

        blogPost = new BlogPost();
        blogPost.setSpace(space);
        blogPost.setTitle("blog");

        calendar = Calendar.getInstance();

        bloggingYear = 2008;
        bloggingMonth = 8;
        bloggingDay = 1;

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(pageManager.getBlogPost(eq(space.getKey()), eq(blogPost.getTitle()), anyObject())).thenReturn(blogPost);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/@news/2008/08/01" + blogPost.getTitle()
        );

        blogPostContentResourceImpl = new BlogPostContentResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                settingsManager, userAccessor, pageManager,
                space.getKey(), bloggingYear, bloggingMonth, bloggingDay, blogPost.getTitle()) {
            @Override
            protected Calendar getBlogPostPublishedDate(int yearPublished, int monthPublished, int dayPublished) {
                return calendar;
            }
        };
    }

    @Test
    public void testExistsIfParentResourceAndBlogExists() {
        when(permissionManager.hasPermission(eq(user), anyObject(), (Object) anyObject())).thenReturn(true);

        assertTrue(blogPostContentResourceImpl.exists());
    }

    @Test
    public void testDoesNotExistIfParentResourceDoesNotExist() {
        assertFalse(blogPostContentResourceImpl.exists());
        verify(permissionManager).hasPermission(eq(user), anyObject(), (Object) anyObject());
    }

    @Test
    public void testUrlContentEncodedWithSpecifiedCharset() throws IOException {
        String expectedOutput = "Blog Post";
        byte[] expectedOutputBytes = expectedOutput.getBytes("UTF-8");

        blogPost.setLastModificationDate(new Date());
        blogPost.setBodyAsString(expectedOutput);

        Settings globalSettings = new Settings();
        globalSettings.setDefaultEncoding("UTF-8");


        when(settingsManager.getGlobalSettings()).thenReturn(globalSettings);

        OutputContext outputContext = mock(OutputContext.class);


        InputStream in = null;
        ByteArrayOutputStream out = null;

        try {
            in = blogPostContentResourceImpl.getContent();
            out = new ByteArrayOutputStream();


            when(outputContext.hasStream()).thenReturn(true);
            when(outputContext.getOutputStream()).thenReturn(out);

            blogPostContentResourceImpl.spool(outputContext);

            assertEquals(
                    expectedOutput,
                    new String(out.toByteArray(), "UTF-8")
            );

            verify(outputContext).setContentLength(expectedOutputBytes.length);
            verify(outputContext).setContentType(blogPostContentResourceImpl.getContentTypeBase());
            verify(outputContext).setModificationTime(blogPost.getLastModificationDate().getTime());
        } finally {
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(in);
        }
    }

    @Test
    public void testCreationTimeEqualsBlogPostCreationTime() {
        Date now = new Date();

        blogPost.setCreationDate(now);
        assertEquals(now.getTime(), blogPostContentResourceImpl.getCreationtTime());
    }

    @Test
    public void testDisplayNameEqualsToBlogPostTitleSuffixedByDotTxt() {
        assertEquals(
                blogPost.getTitle() + ".txt",
                blogPostContentResourceImpl.getDisplayName()
        );
    }
}

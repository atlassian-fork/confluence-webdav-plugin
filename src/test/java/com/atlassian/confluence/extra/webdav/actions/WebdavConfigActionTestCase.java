package com.atlassian.confluence.extra.webdav.actions;

import com.atlassian.confluence.extra.webdav.WebdavSettings;
import com.atlassian.confluence.extra.webdav.WebdavSettingsManager;
import com.opensymphony.xwork.Action;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Set;

import static java.util.Collections.singleton;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WebdavConfigActionTestCase extends TestCase {
    @Mock
    private WebdavSettingsManager webdavSettingsManager;

    private WebdavConfigAction webdavConfigAction;

    private WebdavSettings webdavSettings;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        webdavConfigAction = new WebdavConfigAction() {
            @Override
            public String getText(String s) {
                return s;
            }

            @Override
            public String getText(String s, Object[] objects) {
                return s;
            }
        };


        webdavConfigAction.setWebdavSettingsManager(webdavSettingsManager);

        webdavSettings = new WebdavSettings();
    }

    @Override
    protected void tearDown() throws Exception {
        webdavSettingsManager = null;
        super.tearDown();
    }

    public void testDoDefaultPopulatesActionWithConfigValues() {
        Set<String> denyRegexes = singleton("foo");

        webdavSettings.setExcludedClientUserAgentRegexes(denyRegexes);

        when(webdavSettingsManager.getWebdavSettings()).thenReturn(webdavSettings);

        assertEquals(Action.SUCCESS, webdavConfigAction.doDefault());

        assertTrue(denyRegexes.containsAll(Arrays.asList(webdavConfigAction.getDenyRegexes())));
        assertTrue(webdavConfigAction.isContentUrlResourceEnabled());
        assertTrue(webdavConfigAction.isContentExportsResourceEnabled());
        assertTrue(webdavConfigAction.isContentVersionsResourceEnabled());
    }

    public void testExecuteSaveSettings() {
        webdavConfigAction.setDenyRegexes(new String[]{"foo"});
        webdavConfigAction.setContentUrlResourceEnabled(false);
        webdavConfigAction.setContentVersionsResourceEnabled(false);
        webdavConfigAction.setContentExportsResourceEnabled(false);

        when(webdavSettingsManager.getWebdavSettings()).thenReturn(new WebdavSettings());

        assertEquals(Action.SUCCESS, webdavConfigAction.execute());

        verify(webdavSettingsManager).save(
                argThat(
                        webWebdavSettings -> webWebdavSettings.getExcludedClientUserAgentRegexes().size() == 1
                                && webWebdavSettings.getExcludedClientUserAgentRegexes().contains("foo")
                                && !webWebdavSettings.isContentUrlResourceEnabled()
                                && !webWebdavSettings.isContentVersionsResourceEnabled()
                                && !webWebdavSettings.isContentExportsResourceEnabled()
                )
        );
    }

    public void testInvalidRegexesCausesFieldErrors() {
        webdavConfigAction.setDenyRegexes(new String[]{"(ab"});

        webdavConfigAction.validate();

        assertTrue(webdavConfigAction.hasFieldErrors());
    }

    public void testFieldErrorsNotPresentIfThereIsNoInvalidRegex() {
        webdavConfigAction.setDenyRegexes(new String[]{"ab"});

        webdavConfigAction.validate();

        assertFalse(webdavConfigAction.hasFieldErrors());
    }

    public void testFieldErrorsNotPresentIfThereIsRegex() {
        webdavConfigAction.validate();

        assertFalse(webdavConfigAction.hasFieldErrors());
    }
}

package com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.lock.ActiveLock;
import org.apache.jackrabbit.webdav.lock.LockInfo;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.apache.jackrabbit.webdav.lock.Scope;
import org.apache.jackrabbit.webdav.lock.Type;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class ConfluenceDavSessionTestCase {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private ConfluenceDavSession confluenceDavSession;

    @Mock
    private DavResource davResource;

    @Before
    public void setUp() {
        when(davResource.getResourcePath()).thenReturn("/default/Global/ds/Demonstration Space.txt");
        confluenceDavSession = new ConfluenceDavSession("admin");
    }

    @Test
    public void testSerializable() throws IOException, ClassNotFoundException, DavException {
        Space space = new Space("tst");
        ContentEntityObject contentEntityObject = new Page();
        contentEntityObject.setId(100);

        /* Alter the state of the session for assertions on the session created from serialization later */
        confluenceDavSession.setUserAgent("Test/UserAgent");
        confluenceDavSession.setCurrentlyBeingUsed(true);
        confluenceDavSession.getResourceStates().hideContent(contentEntityObject);
        confluenceDavSession.getResourceStates().hideSpaceDescription(space);

        /* Obtain a lock so that we can know later if lock states are preserved as well */
        LockManager lockManager = confluenceDavSession.getLockManager();
        LockInfo lockInfo = new LockInfo(
                Scope.EXCLUSIVE,
                Type.WRITE,
                confluenceDavSession.getUserName(),
                10000,
                true
        );

        ActiveLock activeLock = lockManager.createLock(
                lockInfo,
                davResource
        );
        String activeLockToken = activeLock.getToken(); /* Locks are identified by tokens, so we get it here */

        confluenceDavSession.addLockToken(activeLockToken);

        /* Serialize first to get the session from deserialization */
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);

        objectOutputStream.writeObject(confluenceDavSession);
        objectOutputStream.flush();

        ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));

        ConfluenceDavSession deserializedConfluenceDavSession = (ConfluenceDavSession) objectInputStream.readObject();

        assertNotSame(confluenceDavSession, deserializedConfluenceDavSession);
        assertThat(deserializedConfluenceDavSession.getUserName(), is(confluenceDavSession.getUserName()));
        assertThat(deserializedConfluenceDavSession.getUserAgent(), is(confluenceDavSession.getUserAgent()));
        assertTrue(deserializedConfluenceDavSession.isCurrentlyBeingUsed());
        assertTrue(deserializedConfluenceDavSession.getResourceStates().isContentHidden(contentEntityObject));
        assertTrue(deserializedConfluenceDavSession.getResourceStates().isSpaceDescriptionHidden(space));

        assertNotSame(confluenceDavSession.getLockManager(), deserializedConfluenceDavSession.getLockManager());
        assertTrue(deserializedConfluenceDavSession.getLockManager().hasLock(
                activeLockToken,
                davResource
        ));

        /* Just to be over-zealous */
        assertFalse(deserializedConfluenceDavSession.getLockManager().hasLock(
                activeLockToken + "-invalid",
                davResource
        ));

        assertThat(deserializedConfluenceDavSession.getLockTokens(), is(confluenceDavSession.getLockTokens()));
    }
}

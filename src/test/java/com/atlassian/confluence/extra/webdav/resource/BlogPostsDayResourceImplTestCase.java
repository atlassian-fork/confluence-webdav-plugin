package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.spaces.Space;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.io.InputContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BlogPostsDayResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    @Mock
    private DavResource davResource;
    @Mock
    private InputContext inputContext;

    private Space space;
    private BlogPostsDayResourceImpl blogPostsDayResourceImpl;

    @Before
    public void initialise() {

        space = new Space("ds");

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/@news/2007/7/7"
        );

        blogPostsDayResourceImpl = new BlogPostsDayResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                userAccessor, permissionManager, pageManager, spaceManager,
                space.getKey(),
                2007, 7, 7);
    }

    /**
     * See <a href="http://developer.atlassian.com/jira/browse/WBDV-142">WBDV-142</a>.
     */
    @Test
    public void testAddMemberNotAllowed() {
        try {
            blogPostsDayResourceImpl.addMember(davResource, inputContext);
            fail("DavException expected here.");
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
        }
    }

    @Test
    public void testRemoveMemberNotAllowed() {
        try {
            blogPostsDayResourceImpl.removeMember(davResource);
            fail("DavException expected here.");
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
        }
    }

    @Test
    public void testMoveNotAllowed() {
        try {
            blogPostsDayResourceImpl.move(davResource);
            fail("DavException expected here.");
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
        }
    }

    @Test
    public void testDisplayNameIsNumericMonth() {
        assertEquals("07", blogPostsDayResourceImpl.getDisplayName());
    }

    @Test
    public void testOnlyBlogPostsOfSpecifiedMonthListedAsChildren() {
        BlogPost blogPost = new BlogPost();
        blogPost.setTitle("testBlog");
        blogPost.setSpace(space);
        blogPost.setCreationDate(new Date());

        when(permissionManager.getPermittedEntities(eq(user), anyObject(), (List) anyObject())).thenReturn(
                singletonList(blogPost)
        );
        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(userAccessor.getConfluenceUserPreferences(user)).thenReturn(confluenceUserPreferences);
        when(confluenceUserPreferences.getTimeZone()).thenReturn(TimeZone.getDefault());

        List<DavResource> members = new ArrayList<DavResource>(blogPostsDayResourceImpl.getMemberResources());

        assertEquals(1, members.size());
        assertTrue(members.get(0) instanceof BlogPostContentResourceImpl);

        verify(pageManager).getBlogPosts(eq(space.getKey()), isA(Calendar.class), eq(Calendar.DATE));
    }
}

package com.atlassian.confluence.extra.webdav.resource;

import org.apache.jackrabbit.webdav.io.OutputContext;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class NonExistentResourceTestCase extends AbstractConfluenceResourceTestCase {
    private NonExistentResource nonExistentResource;

    @Before
    public void initialise() throws Exception {
        super.setUp();

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/doesnotexist"
        );

        nonExistentResource = new NonExistentResource(
                davResourceLocator, davResourceFactory, lockManager, davSession
        );
    }

    @Test
    public void testCreationTimeIsZero() {
        assertEquals(0, nonExistentResource.getCreationtTime());
    }

    @Test
    public void testLockNotSupported() {
        assertNull(nonExistentResource.getSupportedLock());
    }

    @Test
    public void testAlwaysDoesNotExist() {
        assertFalse(nonExistentResource.exists());
    }

    @Test
    public void testAlwaysNotCollection() {
        assertFalse(nonExistentResource.isCollection());
    }

    @Test
    public void testDisplayNameIsLastTokenInPath() {
        assertEquals("doesnotexist", nonExistentResource.getDisplayName());
    }

    @Test
    public void testHasNoChildren() {
        assertEquals(0, nonExistentResource.getMembers().size());
    }

    @Test
    public void testSpoolNothing() throws IOException {
        OutputContext outputContext = mock(OutputContext.class);
        nonExistentResource.spool(outputContext);

        verify(outputContext).setContentLength(0);
    }

}

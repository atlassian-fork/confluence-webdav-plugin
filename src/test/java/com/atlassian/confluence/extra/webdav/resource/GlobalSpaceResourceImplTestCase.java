package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceType;
import com.atlassian.confluence.spaces.SpacesQuery;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.io.InputContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

public class GlobalSpaceResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    @Mock
    private DavResource davResource;
    @Mock
    private InputContext inputContext;

    private GlobalSpacesResourceImpl globalSpacesResourceImpl;

    @Before
    public void initialise() {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath,
                workspacePath + "/Global"
        );

        globalSpacesResourceImpl = new GlobalSpacesResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                spaceManager
        );
    }

    @Test
    public void testMemberAdditionNotAllowed() {
        try {
            globalSpacesResourceImpl.addMember(davResource, inputContext);
            fail("Creation of spaces in the Global resource is not allowed.");
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
        }
    }

    @Test
    public void testRemoveMemberNotAllowed() {
        try {
            globalSpacesResourceImpl.removeMember(davResource);
            fail("Removal of spaces in the Global resource is not allowed.");
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
        }
    }

    @Test
    public void testOnlyGlobalSpacesListedAsChildResources() {
        Space space = new Space("ds");
        when(spaceManager.getAllSpaces(SpacesQuery.newQuery().forUser(user).withSpaceType(SpaceType.GLOBAL).build())).thenReturn(singletonList(space));

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);

        List<DavResource> childResources = new ArrayList<>(globalSpacesResourceImpl.getMemberResources());

        assertEquals(1, childResources.size());
        assertTrue(childResources.get(0) instanceof SpaceResourceImpl);
    }

    @Test
    public void testDisplayName() {
        assertEquals("Global", globalSpacesResourceImpl.getDisplayName());
    }

    @Test
    public void testCreationTimeIsZero() {
        assertEquals(0, globalSpacesResourceImpl.getCreationtTime());
    }
}
